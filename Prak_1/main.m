global g gamma m k;
% Initial values
g = 9.81;
gamma = pi/4;
alpha = 2*pi/9;
v = 1;
h = 2;
m = 1;
k = 0.9;

% Initial values
x_start = 0;
y_start = h;
alpha_start = alpha;
v_start = v;

% before / after final values
x_end = 0;
y_end = 0;
alpha_before = 0;
alpha_after = 0;

vx_before = 0;
vy_before = 0;
v_before = 0;
v_after = 0;

% Draw the fixed angle
figure(1);clf;hold on;

x1 = 0:0.01:2;
y1 = x1*tan(gamma);
plot(x1, y1, 'b');

x2 = -2:0.01:0;
y2 = -x2*tan(gamma);
plot(x2, y2, 'b');

i = 0;

while i < 10
    i = i + 1;
    t_roots = [0,0,0,0];
    
    % Find a final time
    
    % case with a plus (right side)
    koeff_1 = (2*v_start/g)*(cos(alpha_start)*tan(gamma) - sin(alpha_start));
    koeff_2 = (2/g) * (x_start * tan(gamma) - y_start);
    t_roots(1) = 0.5*(-koeff_1 + sqrt(koeff_1 * koeff_1 - 4*koeff_2));
    t_roots(2) = 0.5*(-koeff_1 - sqrt(koeff_1 * koeff_1 - 4*koeff_2)); 
    
    % case with a minus (left side)
    koeff_1 = (2 * v_start/g) * (-cos(alpha_start) * tan(gamma) - sin(alpha_start));
    koeff_2 = (2/g) * (-x_start * tan(gamma) - y_start);
    t_roots(3) = 0.5*(-koeff_1 + sqrt(koeff_1 * koeff_1 - 4*koeff_2));
    t_roots(4) = 0.5*(-koeff_1 - sqrt(koeff_1 * koeff_1 - 4*koeff_2)); 
    
    t_roots = sort(t_roots);
    
    % The third value is the final time
    t_end = t_roots(3);
    
    % Coordinates of the final point
    x_end = x_start + v_start * cos(alpha_start) * t_end;
    y_end = y_start + v_start * sin(alpha_start) * t_end - g*(t_end)*(t_end)/2;
    
    % Velocity in the final point a priori
    vx_before = v_start * cos(alpha_start);
    vy_before = v_start * sin(alpha_start) - g*t_end;
    v_before = sqrt(vx_before*vx_before + vy_before*vy_before);
    
    if (x_end > 0)
        % Case, when the final point is on the right side
        
        % Velocity components in new coordinates BEFORE
        v_tau_before = vx_before * cos(gamma) + vy_before * sin(gamma);
        v_n_before = - vx_before * sin(gamma) + vy_before * cos(gamma);
        
        % Changing the velocity at collision
        v_tau_after = v_tau_before;
        v_n_after = -k*v_n_before;
        
        % Argument of the velocity vector in coordinates (tau, n) 
        beta = atan(v_tau_after / v_n_after);
        if (beta < 0 && v_n_after > 0)
            beta = beta + pi;
        elseif (beta > 0 && v_n_after < 0)
            beta = beta + pi;
        end
        
        % The new argument of the velocity vector in coordinates (x, y)
        alpha_after = beta + gamma;
        
        % The new module of the velocity vector in coordinates (x, y)
        v_after = sqrt(v_tau_after*v_tau_after + v_n_after*v_n_after);
    elseif (x_end < 0)
        % Case, when the final point is on the left side
        
        % Velocity components in new coordinates BEFORE
        v_tau_before = vx_before * cos(pi - gamma) + vy_before * sin(pi - gamma);
        v_n_before = - vx_before * sin(pi - gamma) + vy_before * cos(pi - gamma);
        
        % Changing the velocity at collision
        v_tau_after = v_tau_before;
        v_n_after = -k*v_n_before;
        
        % Argument of the velocity vector in coordinates (tau, n)
        beta = atan(v_tau_after / v_n_after);
        if (beta < 0 && v_n_after > 0)
            beta = beta + pi;
        elseif (beta > 0 && v_n_after < 0)
            beta = beta + pi;
        end
        
        % The new argument of the velocity vector in coordinates (x, y)
        alpha_after = beta + pi - gamma;
        
        % The new module of the velocity vector in coordinates (x, y)
        v_after = sqrt(v_tau_after*v_tau_after + v_n_after*v_n_after);
    else
        % Case, when x_end = 0
        break;
    end

    % Draw the trajectory of the ball
    if (x_start < x_end)
        x3 = x_start:0.0001:x_end;
        y3 = y_start + (x3 - x_start) * tan(alpha_start) - g / (2*(cos(alpha_start))^2*v_start^2) * (x3 - x_start).^2;
        plot(x3, y3, 'r');
    else
        x3 = x_start:-0.0001:x_end;
        y3 = y_start + (x3 - x_start) * tan(alpha_start) - g / (2*(cos(alpha_start))^2*v_start^2) * (x3 - x_start).^2;
        plot(x3, y3, 'r');
    end
    
    % Update the values of variables
    x_start = x_end;
    y_start = y_end;
    v_start = v_after;
    alpha_start = alpha_after;
end 
